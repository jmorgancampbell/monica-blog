---
layout: single
title:  "Photo Gallery Test"
categories: photos
tags: test

gallery:
  - url: /assets/images/2017-09-17-photo-gallery-test/unsplash-gallery-image-1.jpg
    image_path: /assets/images/2017-09-17-photo-gallery-test/unsplash-gallery-image-1-th.jpg
    alt: "placeholder image 1"
    title: "Image 1 title caption"
  - url: /assets/images/2017-09-17-photo-gallery-test/unsplash-gallery-image-2.jpg
    image_path: /assets/images/2017-09-17-photo-gallery-test/unsplash-gallery-image-2-th.jpg
    alt: "placeholder image 2"
    title: "Image 2 title caption"
  - url: /assets/images/2017-09-17-photo-gallery-test/unsplash-gallery-image-3.jpg
    image_path: /assets/images/2017-09-17-photo-gallery-test/unsplash-gallery-image-3-th.jpg
    alt: "placeholder image 3"
    title: "Image 3 title caption"

gallery2:
  - url: /assets/images/2017-09-17-photo-gallery-test/IMG_20170916_174056-ANIMATION.gif
    image_path: /assets/images/2017-09-17-photo-gallery-test/IMG_20170916_174056-ANIMATION.gif
    alt: "placeholder image 1"
    title: "Image 1 title caption"

gallery3:
  - url: /assets/images/2017-09-17-photo-gallery-test/unsplash-gallery-image-1.jpg
    image_path: /assets/images/2017-09-17-photo-gallery-test/unsplash-gallery-image-1-th.jpg
    alt: "placeholder image 1"
    title: "Image 1 title caption"
  - url: /assets/images/2017-09-17-photo-gallery-test/unsplash-gallery-image-1.jpg
    image_path: /assets/images/2017-09-17-photo-gallery-test/unsplash-gallery-image-1-th.jpg
    alt: "placeholder image 1"
    title: "Image 1 title caption"
---

Hi, this is a test of the theme's photo gallery.
{% include gallery class="full" caption="This is a sample gallery with **Markdown support**." %}
I'd write it ehre alsdkjf . alsdkfj . asdlfjkalsdk fjHi, this is a test of the theme's photo gallery. Hi, this is a test of the theme's photo gallery. Hi, this is a test of the theme's photo gallery. Hi, this is a test of the theme's photo gallery. Hi, this is a test of the theme's photo gallery.

Yay, that was fun. How about another gallery?
{% include gallery id="gallery2" class="full" caption="This is a sample gallery with **Markdown support**." %}

Here is the third gallery I want.
{% include gallery id="gallery3" class="full" caption="This is a sample gallery with **Markdown support**." %}
